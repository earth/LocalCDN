## Please note the following tips

* Does the website work after you activate the second switch to filter the HTML source code?

* Is there already an existing issue? (Search for the URL, e.g. "codeberg.org" or "localcdn.org")

* Does the website use a strict SOP? Wiki article: [Broken JavaScript or CSS on some websites](https://codeberg.org/nobody/LocalCDN/wiki/Home#user-content-7-a-website-looks-weird-or-cannot-be-used-if-i-deactivate-localcdn-everything-works-what-is-the-problem)

* Which browser do you use? Chromium-based browsers do not support all features of LocalCDN. Wiki article: [Chromium incompatibilities](https://codeberg.org/nobody/LocalCDN/wiki/Home#user-content-2-can-i-use-this-extension-in-my-chrome-browser)

### If none of the points apply
* delete this template and describe the problem
* write the URL in the title so that other users can find this issue as quickly as possible
* one or two screenshots can also be helpful

Thank you for your understanding
